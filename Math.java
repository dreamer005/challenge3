package Challenge3;

import java.util.List;

public interface Math {
    Integer mode(List<Integer> listValue);
    double mean(List<Integer> listValue);
    double median(List<Integer> listValue);
}
