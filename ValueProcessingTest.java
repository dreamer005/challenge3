package Challenge3;

import Challenge3.ValueProcessing;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ValueProcessingTest {

    ValueProcessing vp = new ValueProcessing();

    // Path
    String pathTrue = "src/main/resources/data_Sekolah.csv";
    String pathFalse = "src/main/resource/data_sekolahsalah.csv";
    String pathNull = null;

    // Name
    String nameFound = "Data Sekolah";
    String nameNotFound = "";
    String nameNull = null;

    @Test
    @DisplayName("Positive Test - Found")
    void testValueProcessingPositive(){
        paramFound();
        testReadVp();
    }

    void paramFound(){
        Assertions.assertDoesNotThrow(() -> vp.valueProcessing(pathTrue, nameFound));
    }

    void testReadVp() {
        Assertions.assertDoesNotThrow( () -> vp.readFile(pathTrue));
    }

    @Test
    @DisplayName("Negative Test - Not Found")
    void testValueProcessingNegative(){
        paramNotFound();
        paramNull();
        testReadVpNotFound();
        testReadVpNull();
    }

    void paramNotFound(){
        IOException i = Assertions.assertThrows(FileNotFoundException.class, ()-> vp.valueProcessing(pathFalse, nameNotFound));
        Assertions.assertEquals("File Not Found!", i.getMessage());
    }

    void testReadVpNotFound() {
        IOException i = Assertions.assertThrows(FileNotFoundException.class, ()-> vp.readFile(pathFalse));
        Assertions.assertEquals("File Not Found!", i.getMessage());
    }

    @Test
    @DisplayName("Negative Test - Null")
    void testValueProcessingNegative2(){
        paramNull();
        testReadVpNull();
    }

    void paramNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, ()-> vp.valueProcessing(pathNull, nameNull));
        Assertions.assertEquals("File is Null", r.getMessage());
    }

    void testReadVpNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, ()-> vp.readFile(pathNull));
        Assertions.assertEquals("File is Null", r.getMessage());
    }
}
