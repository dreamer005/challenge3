package Challenge3;

import Challenge3.ValueGrouping;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ValueGroupingTest {

    ValueGrouping vg = new ValueGrouping();

    // Path
    String pathTrue = "src/main/resources/data_Sekolah.csv";
    String pathFalse = "src/main/resource/data_sekolahsalah.csv";
    String pathNull = null;

    // Name
    String nameFound = "Data Sekolah";
    String nameNotFound = "";
    String nameNull = null;

    @Test
    @DisplayName("Positive Test - Found")
    void testValueGroupingPositive(){
        paramFound();
        testReadVg();
    }

    void paramFound(){
        Assertions.assertDoesNotThrow(() -> vg.frequncy(pathTrue, nameFound));
    }

    void testReadVg() {
        Assertions.assertDoesNotThrow( () -> vg.readFile(pathTrue));
    }

    @Test
    @DisplayName("Negative Test - Not Found")
    void testValueGroupingNegative(){
        paramNotFound();
        paramNull();
        testReadVgNotFound();
        testReadVgNull();
    }

    void paramNotFound(){
        IOException i = Assertions.assertThrows(FileNotFoundException.class, ()-> vg.frequncy(pathFalse, nameNotFound));
        Assertions.assertEquals("File Not Found!", i.getMessage());
    }

    void testReadVgNotFound() {
        IOException i = Assertions.assertThrows(FileNotFoundException.class, ()-> vg.readFile(pathFalse));
        Assertions.assertEquals("File Not Found!", i.getMessage());
    }

    @Test
    @DisplayName("Negative Test - Null")
    void testValueGroupingNegative2(){
        paramNull();
        testReadVgNull();
    }

    void paramNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, ()-> vg.frequncy(pathNull, nameNull));
        Assertions.assertEquals("File is Null", r.getMessage());
    }

    void testReadVgNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, ()-> vg.readFile(pathNull));
        Assertions.assertEquals("File is Null", r.getMessage());
    }
}
