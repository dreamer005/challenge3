package Challenge3;

import Challenge3.PageOne;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

//import java.util.InputMismatchException;


public class PageOneTest {

    PageOne one = new PageOne();
    int inputTrue = 1;
//    int inputFalse = 10;
    int inputMin = -1;
    String inputString = "s";

//    @Test
//    @DisplayName("Positive Test - ")
//    void testPositive() {
//        basePageOneFound();
//    }

    void basePageOneFound() {
//        Assertions.assertDoesNotThrow(() -> one.basePageOne(inputTrue));
    }

    @Test
    @DisplayName("Negative Test - ")
    void testNegative() {
        basePageOneString();
//        basePageOneMin();
//        basePageOneNotFound();
    }

    void basePageOneString() {
        char s = '"';
        RuntimeException r = Assertions.assertThrows(NumberFormatException.class, () -> one.basePageOne(Integer.parseInt(inputString)));
        Assertions.assertEquals("For input string: " + s + inputString + s , r.getMessage());
    }

//    void basePageOneMin() {
//        Assertions.assertThrows(NumberFormatException.class, () -> one.basePageOne(inputMin));
//    }

//    void basePageOneNotFound() {
//        RuntimeException r = Assertions.assertThrows(InputMismatchException.class, () -> one.basePageOne(inputFalse));
//        Assertions.assertEquals("Please input range 0 - 9", r.getMessage());
//    }

}
