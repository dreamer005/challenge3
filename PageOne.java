package Challenge3;

import Challenge3.Generate;
import Challenge3.ValueGrouping;
import Challenge3.FileLocation;

import java.io.IOException;
//import java.util.InputMismatchException;
import java.util.Scanner;

public class PageOne extends Menu{

    Scanner sc = new Scanner(System.in);

    ValueGrouping write = new ValueGrouping();
    FileLocation file = new FileLocation();
    Generate run = new Generate();

    @Override
    public void page() throws IOException{
        basePageOne(secondPage());
    }

    public void basePageOne (int select) throws IOException{
        if (select == 0) {
            run.close();
            System.exit(0);
        } else if (select >= 1 && select <= 9) {
            System.out.print("Create Name File: ");
            write.frequncy(file.location(select), file.nameFile(sc.nextLine()));
            System.out.println();
            System.out.println("Success Writting File");
            System.out.print("Press enter to back main menu");
            sc.nextLine();
        }  else {
//            throw new InputMismatchException("Please input range 0 - 9");
            System.out.println("Please input range 0 - 9");
            System.out.println("Correct Re-input 'PRESS ENTER'");
            sc.nextLine();
            page();
        }
    }
}
