package Challenge3;

import Challenge3.ValueProcessing;
import Challenge3.FileLocation;

import java.io.IOException;
import java.util.Scanner;

public class PageTwo extends Menu{

    ValueProcessing write = new ValueProcessing();
    Scanner sc = new Scanner(System.in);
    FileLocation file = new FileLocation();

    @Override
    public void page() throws IOException{
        basePageTwo(secondPage());
    }

    public void basePageTwo(int select) throws IOException {
        if (0 == select) {
            System.exit(0);
        } else if (select >= 1 && select <= 9) {
            file.location(select);
            System.out.print("Create Name File: ");
            write.valueProcessing(file.location(select), file.nameFile(sc.nextLine()));
            System.out.println();
            System.out.println("Success Writting File");
            System.out.print("Press enter to back main menu");
            sc.nextLine();
        } else {
//            throw new InputMismatchException("Please input range 0 - 9");
            System.out.println("Please input range 0 - 9");
            System.out.println("Correct Re-input 'PRESS ENTER'");
            sc.nextLine();
            page();
        }
    }
}
