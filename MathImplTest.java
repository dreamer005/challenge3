package Challenge3;

import Challenge3.MathImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MathImplTest {
    MathImp search = new MathImp();
    DecimalFormat df = new DecimalFormat("0.00");

    ArrayList listNull = null;
    ArrayList listKosong;
    String expectedError = "List is Null!";

    List<Integer> listAngka() {
        List<Integer> listSchool = new ArrayList<>(
                List.of(7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,5,6,7,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,7,7,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,6,7,8,
                7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,7,7,9,9,8)
        );
        Collections.sort(listSchool);
        return listSchool;
    }


    @Test
    @DisplayName("Positive Test - List Found")
    void testMathImplPo() {
        testMean();
        testMode();
        testMed();
    }

    void testMean() {
        double expectedMean = 8.32;
        Assertions.assertEquals(df.format(expectedMean), df.format(search.mean(listAngka())));
    }

    void testMode() {
        double expectedMode = 7;
        Assertions.assertEquals(df.format(expectedMode), df.format(search.mode(listAngka())));
    }

    void testMed() {
        double expectedMed = 8;
        Assertions.assertEquals(df.format(expectedMed), df.format(search.median(listAngka())));
    }

    @Test
    @DisplayName("Negative Test - List is Null")
    void testMathImplNegative() {
        testMeanNull();
        testMedNull();
        testModeNull();
    }

    void testMeanNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.mean(listNull)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

    void testMedNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.median(listNull)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

    void testModeNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.mode(listNull)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

    @Test
    @DisplayName("Negative Test - List Kosong")
    void testMathImplNegative2() {
        testMeanKosong();
        testMedKosong();
        testModeKosong();
    }

    void testMeanKosong() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.mean(listKosong)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

    void testMedKosong() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.median(listKosong)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

    void testModeKosong() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> df.format(search.mode(listKosong)));
        Assertions.assertEquals(expectedError, r.getMessage());
    }

}
