package Challenge3;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileLocation {

    // Mencari File
    public String location(int select) throws RuntimeException{

        File srcFile = new File("src/main/tempfile");

        List<String> src = new ArrayList<>();
        src.add("src/main/resources/data_Sekolah.csv");

        String[] value = srcFile.list();

        if (value == null) {
            throw new NullPointerException("Value is Null");
        }else {
            for (String a : value) {
                String srcFolder = "src/main/tempfile/";
                String srcFinal = srcFolder+a;
                src.add(srcFinal);
            }
            if (select > src.size()){
                throw new IndexOutOfBoundsException("Index Not Found!");
            }else if (select < 0) {
                throw new IndexOutOfBoundsException("Index Can't Minus");
            }else {
                return (src.get(select - 1));
            }
        }
    }

    // Akan Tersimpan Disini "src/main/savefile/"
    public String nameFile(String nameFile) throws RuntimeException{
        String src = "src/main/savefile/";
        if (nameFile == null) {
            throw new NullPointerException("Name File Can't Null");
        }else {
            return src + nameFile + ".txt";
        }
    }
}
