package Challenge3;

//import com.app.menu.Main;

import java.util.*;

public class MathImp implements Math{

//    Main main = new Main();

    /*-------------------------MODE-------------------------*/
    @Override
    public Integer mode(List<Integer> listValue) throws RuntimeException{
        if (listValue == null){
//            main.showMenu();
            throw new NullPointerException("List is Null!");
        } else {
            List<Integer> tempValues = new ArrayList<>(listValue);

            Set<Integer> diffValue = new HashSet<>(tempValues);

            Map<Integer, Integer> distribValue = new HashMap<>();

            for (Integer a : diffValue) {
                Integer freq = 0;
                for (Integer b : tempValues) {
                    if (a.equals(b)) {
                        freq++;
                    } else if (b > a) {
                        break;
                    }
                }
                distribValue.put(freq, a);
            }
            Integer maxKey = Collections.max(distribValue.keySet());
            return distribValue.get(maxKey);
        }
    }

    /*-------------------------MEAN-------------------------*/
    @Override
    public double mean(List<Integer> listValue) throws RuntimeException{
        double mean;
        double sum = 0;
        if (listValue == null){
//            main.showMenu();
            throw new NullPointerException("List is Null!");
        } else {
            for (Integer integer : listValue) {
                sum += integer;
            }
            mean = sum / listValue.size();
            return mean;
        }

    }

    /*-------------------------MEDIAN-------------------------*/
    @Override
    public double median(List<Integer> listValue) throws RuntimeException{
        double median;
        if (listValue == null){
//            main.showMenu();
            throw new NullPointerException("List is Null!");
        } else {
            if (listValue.size() % 2 == 0){
                median = (double)( listValue.get(listValue.size() / 2 ) + listValue.get(listValue.size() / 2 + 1 )) / 2;
            }
            else {
                median = (double) listValue.get(listValue.size() / 2);
            }
            return median;
        }
    }
}
