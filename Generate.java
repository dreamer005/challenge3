package Challenge3;

import java.io.*;

final public class Generate {

    //Menghapus File yang telah digenerate
    public void close(){
        File deletefl = new File("src/main/tempfile");

        String[] s = deletefl.list();

        if (s == null) {
            throw new IndexOutOfBoundsException("Index is Null");
        } else {
            for (String a : s){
                File f1 = new File(deletefl,a);
                f1.delete();
            }
            System.out.println("SHUTDOWN AND DELETING TEMP FILE SUCCESSFULL... ");
        }
    }

    //Membaca File Kelas A - H
    public void openGenerate(String path) throws IOException, RuntimeException{

        try {
            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            // src save data kelas
            PrintStream a = new PrintStream("src/main/tempfile/kelasA.csv");
            PrintStream b = new PrintStream("src/main/tempfile/kelasB.csv");
            PrintStream c = new PrintStream("src/main/tempfile/kelasC.csv");
            PrintStream d = new PrintStream("src/main/tempfile/kelasD.csv");
            PrintStream e = new PrintStream("src/main/tempfile/kelasE.csv");
            PrintStream f = new PrintStream("src/main/tempfile/kelasF.csv");
            PrintStream g = new PrintStream("src/main/tempfile/kelasG.csv");
            PrintStream h = new PrintStream("src/main/tempfile/kelasH.csv");

            String line = " ";

            // Read per line
            char kelas = 'A';
            while ((line = br.readLine()) != null) {

                // write and save data per kelas
                if (kelas == 'A'){
                    a.println(line);
                } else if (kelas == 'B'){
                    b.println(line);
                } else if (kelas == 'C'){
                    c.println(line);
                } else if (kelas == 'D'){
                    d.println(line);
                } else if (kelas == 'E'){
                    e.println(line);
                } else if (kelas == 'F'){
                    f.println(line);
                } else if (kelas == 'G'){
                    g.println(line);
                } else if (kelas == 'H') {
                    h.println(line);
                }
                kelas ++;
            }
            br.close();
        } catch(IOException i) {
            throw new FileNotFoundException("File Not Found!");
        } catch (RuntimeException r) {
            throw new NullPointerException("File is Null");
        } finally {
            System.out.println("\n\nGENERATING SUCCESSFULL...");
            System.out.print("\n\nPRESS ENTER TO CONTINUE");
        }
    }
}
