package Challenge3;

import java.io.*;
import java.util.*;

public class ValueGrouping extends ReadAction {

    @Override
    public void read() {}

    /*-------------------------WRITE FREQUENCY-------------------------*/
    public void frequncy(String path, String nameFile) throws IOException, RuntimeException {
        try {
            File file = new File(nameFile);
            if(path != null && file.createNewFile()) {
                System.out.println("File tersimpan di: " + nameFile);
            } else if (file.exists()) {
                System.out.println("File Telah di Update");
                System.out.println("File tersimpan di: " + nameFile);
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            List<Integer> tempValues = new ArrayList<>(readFile(path));

            Set<Integer> diffValue = new HashSet<>(tempValues);

            Map<Integer,Integer> distribValue = new HashMap<>();

            bwr.write("Berikut Hasil Pengolahan Nilai:\n");
            bwr.write("\n");
            bwr.write("Nilai\t\t" + "|\t\t" + "Frekuensi\n");
            bwr.write("\n");

            for ( Integer a : diffValue) {
                Integer freq = 0;
                for ( Integer b : tempValues){
                    if (a.equals(b)){
                        freq ++;
                    } else if ( b > a){
                        break;
                    }
                }
                distribValue.put(freq, a);
                bwr.write(distribValue.get(freq) + "\t\t\t" + "|\t\t" + freq + "\n");
            }
            bwr.flush();
            bwr.close();
        } catch(IOException i) {
            throw new FileNotFoundException("File Not Found!");
        } catch (RuntimeException r) {
            throw new NullPointerException("File is Null");
        }
    }
}
