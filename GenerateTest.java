package Challenge3;

import Challenge3.Generate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

public class GenerateTest {

    Generate gr = new Generate();

    // Path
    String pathTrue = "src/main/resources/data_Sekolah.csv";
    String pathFalse = "src/main/resource/data_sekolahsalah.csv";
    String pathNull = null;

//    PrintStream a = new PrintStream("src/main/tempfile/kelasA.csv");

    @Test
    @DisplayName("Positve Test - File Found")
    void testPositive() {
        pathFound();
    }

    void pathFound() {
        Assertions.assertDoesNotThrow(() -> gr.openGenerate(pathTrue));
    }

    @Test
    @DisplayName("Negative Test - File Not Found")
    void testNegative() {
        pathNotFound();
    }

    void pathNotFound() {
        IOException i = Assertions.assertThrows(FileNotFoundException.class, () -> gr.openGenerate(pathFalse));
        Assertions.assertEquals("File Not Found!", i.getMessage());
    }

    @Test
    @DisplayName("Negative Test - File Null")
    void testNegative2() {
        pathNotFound();
        pathNull();
    }

    void pathNull() {
        RuntimeException r = Assertions.assertThrows(NullPointerException.class, () -> gr.openGenerate(pathNull));
        Assertions.assertEquals("File is Null", r.getMessage());
    }
}
