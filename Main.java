package Challenge3;

import Challenge3.Generate;
import Challenge3.FileLocation;

//import java.util.InputMismatchException;

public class Main extends Menu{
    public void showMenu() {
        PageOne pageOne = new PageOne();
        PageTwo pageTwo = new PageTwo();
        Generate run = new Generate();
        FileLocation file = new FileLocation();


        try {
            do {
                header();
                run.openGenerate(file.location(1));
                sc.nextLine();
                switch (mainPage()) {
                    case "0":{
                        run.close();
                        System.exit(0);
                    }
                    case "1":{
                        try {
                            pageOne.page();
                        } catch (Exception e) {
//                            throw new InputMismatchException("You cannot enter letters or null");
                            pageOne.page();
                        }
                        break;
                    }

                    case "2":{
                        try {
                            pageTwo.page();
                        } catch (Exception e) {
//                            throw new InputMismatchException("You cannot enter letters or null");
                            pageTwo.page();
                        }
                        break;
                    }

                    default:{
//                        throw new InputMismatchException("You cannot enter letters or null");
                        System.out.println("Please input range 0 - 2");
                        System.out.print("Correct Re-input 'PRESS ENTER'");
                        sc.nextLine();
                    }
                }
            }while (true);
        } catch (Exception e){
//            throw new InputMismatchException("You cannot enter letters or null");
            sc.nextLine();
            mainPage();
        }

    }

    @Override
    public void page() {
        showMenu();
    }
}
